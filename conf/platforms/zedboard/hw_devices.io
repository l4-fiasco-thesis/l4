-- vim:set ft=lua:
-- (c) 2014 Emmanouil Fragkiskos Ragkousis.
-- This file is distributed under the terms of the
-- GNU General Public License 2.
-- Please see the COPYING-GPL-2 file for details.

local Hw = Io.system_bus()

Io.add_vbus("l4linux", Io.Vi.System_bus
{
   slctrl = wrap(Hw.SLCR);
   ethernet = wrap(Hw.NIC);
   sdhci = wrap(Hw.MMC);
   dmacontroller = wrap(Hw.DMAC);
   bram = wrap(Hw.FPGA_BRAM);
   -- usbcontroller = wrap(Io.system_bus():match("chipidea,usb2"));
})
